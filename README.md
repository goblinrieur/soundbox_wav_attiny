# soundbox_wav_attiny

This sound-box can read stereo wav files (_48KHz optimized_) using a uniq button to read next sound file.

It is powered by a single 3.3v to 3.7v battery & uses 2 standard 8 ohms speakers to output the sound.

It is build to be cheap & a gadget, not for a good sound, not for space or volume optimisation & so on.

```
        ___   ___   ___  
__   __/ _ \ / _ \ / _ \ 
\ \ / / | | | | | | | | |
 \ V /| |_| | |_| | |_| |
  \_/  \___(_)___(_)___/ 
```
                         
[original project](./originalproject)

# my version below

![schematic](./2023-03-30_15-39.png)

![pcb face A](./dfs.png)

![pcb face B](./dfs2.jpg)

[KICAD FILES](./KICAD_PCB)
